import os
from setuptools import setup, find_packages

src_dir = os.path.join(os.getcwd(), 'src')
packages = {"" : "src"}
for package in find_packages("src"):
    packages[package] = "src"

with open('requirements.txt') as reqs:
    requirements = reqs.read()

setup(
    packages = packages.keys(),
    package_dir = {"" : "src"},
    name = 'mcvae',
    version = '1.0.1',
    author = 'Luigi Antelmi',
    author_email = 'luigi.antelmi@inria.fr',
    description = 'Multi-Channel Variational Autoencoder',
    long_description = 'TODO',
    license = 'Inria',
    install_requires=requirements,
    )
